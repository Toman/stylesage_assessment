import csv
import re
import sqlite3
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from os.path import join, dirname
from textwrap import dedent
from typing import List
import requests
from bs4 import BeautifulSoup
from data_enrichment import add_extra_info
from sqlite3 import Error
from stylesage_api import establish_api

WEB_URL = 'https://www.imdb.com/chart/tvmeter'
IMDB_URL = 'https://www.imdb.com'


def write_csv(content: List[dict], path: str):
    """
    Writes input to csv file.
    :param content: Data to be written to csv.
    :param path: Path to the output file
    """
    with open(path, 'w') as csvFile:
        writer = csv.DictWriter(csvFile, fieldnames=content[0].keys(), delimiter=';')
        writer.writeheader()
        writer.writerows(content)
    print("Data saved as CSV. \n")


def extract_data_website(url: str, proxy: str = None) -> BeautifulSoup:
    """
    Access and scraped data from provided website/url.
    :param url: URL of website
    :param proxy: Proxy string, optional
    :return: BS object(s)
    """
    if proxy:
        print("Using proxy: ", proxy)
        proxy = {
            "http": "http://" + proxy,
            "https": "https://" + proxy,
        }
    try:
        print("Extracting data from website -", url)
        soup = BeautifulSoup(requests.get(url, proxies=proxy).text, 'html.parser')
    except requests.exceptions.ProxyError as e:
        print("Can't access {url} using proxy. \n ERROR \n: {message}".format(url=url, message=e))

    item_list = soup.find('tbody', class_='lister-list')
    print("Scraping done. \n")
    return item_list.find_all('tr')


def extract_data_info(data: BeautifulSoup) -> dict:
    """
    Extract desired data from source and transforms them to desired output - dictionary.
    :param data: Source of data for further extraction
    :return: Dict of containing extracted data
    """
    title_tag = data.find('td', class_='titleColumn')
    title_link = title_tag.find('a', {'title': True})
    release_data = title_tag.find('span', class_='secondaryInfo').text
    premiere = re.search(r'(\d{4})', release_data).group(1)  # dummy regex to get year info
    popularity_tag = data.find('td', class_='posterColumn')
    popularity = popularity_tag.find('span', {'name': 'rk'})['data-value']
    rating = popularity_tag.find('span', {'name': 'ir'})['data-value']

    store_dict = {'name': title_link.text,
                  'cast': title_link['title'],
                  'imdb_url': IMDB_URL + title_link['href'],
                  'premiered_in': premiere,
                  'current_imdb_rank': popularity,
                  'user_rating': rating
                  }
    return store_dict


def load_data_to_sqlite(db_file: str, db_content: List[dict], table_name: str):
    """
    Creates db if not doesn't exists and inserts data to the table.
    :param db_file: Path/Name of db file
    :param db_content: Data to be inserted
    :param table_name: Table to insert data into
    """
    try:
        conn = sqlite3.connect(db_file)

        cursor = conn.cursor()
        cursor.execute('DROP TABLE IF EXISTS {t_name}'.format(t_name=table_name))
        cursor.execute('CREATE TABLE {t_name} '
                        '(description VARCHAR, '
                        'current_imdb_rank VARCHAR, '
                        'status VARCHAR,'
                        'imdb_url VARCHAR,'
                        'series_url VARCHAR,'
                        'cast VARCHAR,'
                        'name VARCHAR,'
                        'user_rating VARCHAR,'
                        'genre VARCHAR,'
                        'premiered_in VARCHAR )'.format(t_name=table_name))
        column_names = list({k for item in db_content for k in item.keys()})
        column_names_str = ', '.join(column_names)
        binds_str = ', '.join('?' for _ in range(len(column_names)))
        print("Inserting data to db... \n")
        for data_dict in db_content:
            sql = ("INSERT INTO {t_name} ({columns_names}) "
                   "VALUES ({binds})"
                   .format(t_name=table_name,
                           columns_names=column_names_str,
                           binds=binds_str))
            values = [data_dict.get(column_name, '')
                      for column_name in column_names]
            cursor.execute(sql, values)
        print("Insert successful. \n")
        conn.commit()

    except Error as e:
        print("Sqlite Error: \n", e)
    finally:
        cursor.close()
        conn.close()


def scrape_and_enrich(args):
    """
    Extracts data from website and enriches them.
    :param args: Input params for program execution
    """
    scraped_data = extract_data_website(WEB_URL, args.proxy)
    extracted_data = [extract_data_info(store) for store in scraped_data]
    enriched_data = add_extra_info(extracted_data)

    load_data_to_sqlite(join(dirname(__file__), args.database + '.db'), enriched_data, args.output)
    if args.csv:
        out_csv_file = join(dirname(__file__), args.output + '.csv')
        write_csv(enriched_data, out_csv_file)


if __name__ == "__main__":
    parser = ArgumentParser(
        prog='StyleSage Assessment',
        formatter_class=RawDescriptionHelpFormatter,
        epilog=dedent('''\
                     Scrapes first page of most popular series on imdb.com,
                     extracts desired data which are further enriched using
                     tvmaze public API and stored as sqlite db (and csv).
                     
                     Example of command to run program:
                     --- START OF EXAMPLE ---
                     python3 most_popular_series stylesage -c
                     --- END OF EXAMPLE ---
                     
                     following example stores data into stylesage.db file in
                     a table 'most_popular_series' and to CSV (-c switch) file
                     having same name as table.
                     
                     --- API RESOURCES ---
                     /series/all  -- fetches the whole list of series/all the data
                     /series/top/<limit>   -- fetches top <limit> ranked series
                     /series/name/<series_name>  -- fetches info about single series <series_name>
                     /series/rating/<rating> -- fetches series having user rating greater then <rating> 
                     --- API RESOURCES ---
                     '''))

    parser.add_argument("output", help="name of the output file (table in db)")
    parser.add_argument("database", help="sqlite db to connect to")
    parser.add_argument("-x", "--proxy", type=str, help="proxy string for scraping, IP:PORT")
    parser.add_argument("-c", "--csv", help="if used, output will be saved as csv file as well")
    parser.add_argument("-p", "--port", default=5002, help="port used to connect to API, default being '5002'")
    scrape_and_enrich(parser.parse_args())  # scraping + enriching
    establish_api(parser.parse_args())  # api

