from time import sleep
import json
from tqdm import tqdm
import requests
from typing import List
from bs4 import BeautifulSoup

TVMAZE_API = 'http://api.tvmaze.com/search/shows?q={series}'


def add_extra_info(content: List[dict]) -> List[dict]:
    """
    Adds extra data to scraped data, data enriching.
    :param content: Data to enrich.
    :return: Enriched data.
    """
    print('\nEnriching data ... \n')
    for item in tqdm(content):
        req = requests.get(TVMAZE_API.format(series=item['name']))
        json_resp = json.loads(req.text)
        try:
            data = json_resp[0]['show']
        except IndexError:  # inconsistent responses from API, bit hacky
            data = json_resp

        try:
            item['series_url'] = data['officialSite']
            item['genre'] = ', '.join(genre for genre in data['genres'])
            item['description'] = BeautifulSoup(data['summary'], 'html.parser').text
            item['status'] = data['status']
        # occurs when not using proxy, API doesn't recognize some series when they have name in Spanish
        except TypeError:
            pass

        sleep(0.2)  # minor sleep to prevent timeout, not really necessary

    print('\n Enriching done. \n')
    return content