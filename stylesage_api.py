from flask import Flask
from flask_jsonpify import jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine


def establish_api(args):
    """
    Connects to a database and provides API to access the data.
    :param args: Input params to run the program.
    """
    db_connect = create_engine('sqlite:///{db_name}.db'.format(db_name=args.database))
    app = Flask(__name__)
    api = Api(app)
    table_name = args.output

    class SeriesName(Resource):
        def get(self, series_name):
            conn = db_connect.connect()
            query = conn.execute("SELECT * FROM {t_name} WHERE name= '{f_name}'".format(t_name=table_name,
                                                                                        f_name=series_name))
            return jsonify(query.cursor.fetchall())

    class FetchAll(Resource):
        def get(self):
            conn = db_connect.connect()
            query = conn.execute("SELECT * FROM {t_name}".format(t_name=table_name))
            return {'series': [i for i in query.cursor.fetchall()]}

    class FetchTop(Resource):
        def get(self, limit):
            conn = db_connect.connect()
            query = conn.execute("SELECT * FROM {t_name} ORDER BY current_imdb_rank LIMIT {limit}"
                                 .format(t_name=table_name,
                                         limit=limit))
            return {'series': [i for i in query.cursor.fetchall()]}

    class FetchUserRating(Resource):
        def get(self, rating):
            conn = db_connect.connect()
            query = conn.execute("SELECT * FROM {t_name} WHERE user_rating > {rating}"
                                 .format(t_name=table_name,
                                         rating=rating))
            return {'series': [i for i in query.cursor.fetchall()]}

    api.add_resource(FetchAll, '/series/all')
    api.add_resource(FetchTop, '/series/top/<limit>')
    api.add_resource(SeriesName, '/series/name/<series_name>')
    api.add_resource(FetchUserRating, '/series/rating/<rating>')

    app.run(port=str(args.port))