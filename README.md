# Stylesage Assessment

## Short description 
I decided to scrape most popular series on imdb.com <https://www.imdb.com/chart/tvmeter> and enrich those using 
public API <http://www.tvmaze.com/api> found here -> <https://github.com/toddmotto/public-apis>.
Enriching consists of adding extra data to scraped data (series url, description, status, genre).
I've provided an option to run this program using proxy, to ensure you can enrich all data as
imdb returns scraping results in Spanish cause of my IP and that difference in language of results
may cause some of the series won't be enriched. To avoid use proxy (preferably english speaking countries),
sadly I couldn't test this as all the (free) proxies I've tried were blocked (that was to be expected...).

After enriching, program saves data to sqlite3 db file (and csv, if you choose to) and establishes locally
available API.

### Files
- `stylesage_assessment.py` Runs the whole program
- `data_enrichment.py` Script for data enrichment
- `stylesage_api.py` Establishes API connection
- `requirements.txt` requirements for pip

### Usage

`python3 stylesage_assessment.py <output> <database> -x <IP:PORT> -c -p <port>`

 - `<output>` table name in database/name of csv file
 - `<database>` path to db file to connect to, if not provided it will created
 - `-x <IP:PORT>` use of proxy, provided as string of IP address and port
 - `-c` switch to save results as csv
 - `-p <port>` switch to specify port for API connection, default being 5002
 
 ##### Example
 - `python3 stylesage_assessment.py most_popular_series stylesage -c`
 - Runs the program using 'stylesage.db', inserting to table 'most_popular_series'
 and saves output as CSV called 'most_popular_series.csv'
 
 ### API Resources
 - `/series/all` -- fetches the whole list of series/all the data
 - `/series/top/<limit>` -- fetches top <limit> ranked series
 - `/series/name/<series_name>` -- fetches info about single series <series_name>
 - `/series/rating/<rating>` -- fetches series having user rating greater then <rating>
 
 ##### Examples
 - `/series/top/3`
 - `/series/name/Chambers`
 - `/series/rating/9.3`
 
- /series/rating/9.3 ![](./stylesage.png)
 
 
 